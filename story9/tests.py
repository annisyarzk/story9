from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime

class unitTest(TestCase):
	def test_ada_url_kosong(self):
		c = Client()
		response = c.get('/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_ada_login_page(self):
		c = Client()
		response = c.get("/login/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_ada_landing_page(self):
		c = Client()
		response = c.get("/landing/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_basehtml(self):
		c = Client()
		response = c.get("/")
		self.assertTemplateUsed(response, 'base.html')
		self.assertTemplateUsed(response, 'main.html')
		self.assertTemplateUsed(response, 'navbar.html')

	# def test_apakah_ada_button_login(self):
	# 	c = Client()
	# 	response = c.get('/login')
	# 	content = response.content.decode('utf8')
	# 	self.assertIn("<button", content)
	# 	self.assertIn("login", content) 

	def test_ada_gambar(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("<img", content)

# class functionalTest(LiveServerTestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')        
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(functionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(functionalTest, self).tearDown()

# 	def test_apakah_tombol_login_dapat_diklik(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)
# 		time.sleep(3)
# 		tmp_login = selenium.find_element_by_id('submit')
# 		time.sleep(3)
# 		tmp_login.send_keys(Keys.RETURN)

# 	def test_form_login(self):
# 		selenium = self.selenium
# 		selenium.get('http://localhost:8000/login/')
# 		time.sleep(3)
# 		tmp_username = selenium.find_element_by_id('id_username')
# 		tmp_pass = selenium.find_element_by_id('id_password')
# 		time.sleep(3)

# 		tmp_username.send_keys('unintended')
# 		time.sleep(3)
# 		tmp_pass.send_keys('emomuse99')