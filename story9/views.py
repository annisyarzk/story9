from django.shortcuts import render, redirect
import requests

def main(request):
	return render(request,'main.html')

def landing(request):
	return render(request,'registration/landing.html')
