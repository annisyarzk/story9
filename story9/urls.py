from django.urls import include, path
from . import views
from django.shortcuts import render
from django.conf.urls import url

app_name = 'story9'
urlpatterns = [
	path('',views.main,name='main'),
	path('landing/',views.landing,name='landing'),
]